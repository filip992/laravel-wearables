<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WearableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = ['Mark One', 'Mark Two','Mark Three'];
        for ($i=0; $i < 100; $i++) {

            DB::table('wearables')->insert(
                [
                    'model_name' => $models[rand(0,2)],
                    'uuid' => Str::uuid(),
                    'current_battery_level' => rand(0, 100),
                ]);
        }
    }
}
