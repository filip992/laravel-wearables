<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/allwearables', [\App\Http\Controllers\WearablesController::class, 'index']);
Route::get('/singlewearable', [\App\Http\Controllers\WearablesController::class, 'show']);
Route::post('/sync', [\App\Http\Controllers\BatteryDataController::class, 'sync']);
Route::post('/createwearable', [\App\Http\Controllers\WearablesController::class, 'create']);

Route::get('/allbatterydata', [\App\Http\Controllers\BatteryDataController::class, 'index']);
Route::get('/singlebatterydata', [\App\Http\Controllers\BatteryDataController::class, 'show']);



Route::group(['middleware' => 'throttle:3,10'], function () {

});