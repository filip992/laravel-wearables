<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Wearable extends Model
{



    protected $hidden = array('id');

    protected $casts = [

        'uuid' => 'string',
        'current_battery_level' => 'integer',
        'model_name' => 'string'

    ];


    public function history ()
    {
        return $this->hasMany(BatteryData::class,'wearable_id','id');
    }


}
