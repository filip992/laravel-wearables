<?php

namespace App\Http\Controllers;

use App\Models\BatteryData;
use App\Models\Wearable;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WearablesController extends Controller
{

    public function index()
    {
        return response(Wearable::all());
    }

    public function show(Request $request)
    {
        $request->validate([
            'uuid' => 'required|exists:wearables,uuid|string|size:36'
        ]);


        return response(Wearable::where('uuid',$request->uuid)->get());
    }

    public function create(Request $request)
    {
        $request->validate([
            'model_name' => 'required|in:Mark One,Mark Two,Mark Three',
            'current_battery_level' => 'sometimes|integer|between:0,100'
        ]);



        $wearable = new Wearable();
        $wearable->model_name = $request->model_name;
        $wearable->uuid = Str::uuid();

        if($request->has("current_battery_level")) {

            $wearable->current_battery_level = $request->current_battery_level;
            $wearable->save();

            $batteryData = new BatteryData();
            $batteryData->battery_level = $request->current_battery_level;
            $batteryData->wearable_id = $wearable->id;
            $batteryData->save();

        }
        else {
            $wearable->current_battery_level = 100;
            $wearable->save();
        }

        return response()->json([
            'status' => 'success',
            'data' => $wearable,
        ]);
    }

}
