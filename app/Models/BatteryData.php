<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatteryData extends Model
{
    public $timestamps = ["updated_at"];
    const CREATED_AT  = null;

    protected $hidden = array('wearable_id','id');
    // protected $table = 'wearables';
    protected $casts = [

        'battery_level' => 'integer',

    ];


}
