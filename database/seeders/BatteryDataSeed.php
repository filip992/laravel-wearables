<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class BatteryDataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=0; $i < 500; $i++) {

            DB::table('battery_data')->insert(
                [
                    'wearable_id' => rand(1, 100),
                    'battery_level' => rand(1, 100),
                    'updated_at' => Carbon::today()->subDays(rand(0, 365))->subMinutes(rand(0, 59))->subSeconds(rand(0, 59)),
                ]);
        }
    }
}
