<?php

namespace App\Http\Controllers;

use App\Models\BatteryData;
use App\Models\Wearable;
use Illuminate\Http\Request;

class BatteryDataController extends Controller
{
    public function index()
    {
       return(Wearable::with('history')->get());
    }


    public function sync(Request $request)
    {
        $request->validate([
            'uuid' => 'required|exists:wearables,uuid|string|size:36',
            'battery_level' => 'required|integer|between:0,100'
        ]);

        $wearable = Wearable::where('uuid', '=', $request->uuid)->first();
        $wearable->current_battery_level = $request->battery_level;
        $wearable->save();

        $batteryData = new BatteryData();
        $batteryData->battery_level = $request->battery_level;
        $batteryData->wearable_id = $wearable->id;

        $batteryData->save();


        return response()->json([
            'status' => 'success',
            'data' => $batteryData,
        ]);
    }

    public function show(Request $request)
    {
        $request->validate([
            'uuid' => 'required|exists:wearables,uuid|string|size:36'
        ]);

        return response(

            Wearable::with('history')
                ->where('uuid','=',$request->uuid)
                ->get()
        );
    }
}
